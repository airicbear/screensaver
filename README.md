# Screensaver

## Description
Screensaver with movable players

## Date
September 24, 2017

## Teacher
Mr. Sharick

## Notes
I DREW PICTURE OF DOGE :D  
The framerate depends on location of the player  
## Controls (no caps lock)
`1` - Select triangle  
`2` - Select Doge  
`w` - Move player up  
`a` - Move player left  
`s` - Move player down  
`d` - Move player right  
`f` - hide player (only works on non-image players)  
`r` - reset player  
`q` - increase player size  
`e` - decrease player size  
Left Mouse Button - fire projectile  
Right Mouse Button - set player position to mouse position
